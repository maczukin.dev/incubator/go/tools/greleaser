# GReleaser

A small, simple tool to release projects as GitLab Release, with the assets stored
at GitLab's Generic Package repository

## Author

Tomasz Maczukin, 2023

## License

MIT

