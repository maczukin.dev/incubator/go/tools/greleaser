package main

import (
	"os"

	"github.com/spf13/cobra"

	"go.maczukin.dev/tools/greleaser"
)

const (
	releasePackageIDFlag = "package-id"
	releaseTagFlag       = "tag"
	releaseVersionFlag   = "version"
)

func getDeleteCmd() (*cobra.Command, error) {
	cmd := &cobra.Command{
		Use:   "delete",
		Short: "Delete the release",
		RunE:  deleteRelease,
	}

	err := addCommonFlags(cmd)
	if err != nil {
		return nil, err
	}

	releasePackageID, err := getInt64Env(greleaser.ReleasePackageIDEnv, -1)
	if err != nil {
		return nil, err
	}

	cmd.PersistentFlags().Int64(releasePackageIDFlag, releasePackageID, "ID of the Generic Package that should be deleted")
	cmd.PersistentFlags().String(tagFlag, os.Getenv(greleaser.ReleaseTagEnv), "Tag related to the release. When available, GReleaser will try to delete the GitLab Release entry.")
	cmd.PersistentFlags().String(releaseVersionFlag, os.Getenv(greleaser.ReleaseVersionEnv), "Version of the release")

	return cmd, nil
}

func deleteRelease(cmd *cobra.Command, _ []string) error {
	r, err := getReleaser(cmd)
	if err != nil {
		return err
	}

	packageID, err := cmd.PersistentFlags().GetInt64(releasePackageIDFlag)
	if err != nil {
		return err
	}

	tag, err := cmd.PersistentFlags().GetString(releaseTagFlag)
	if err != nil {
		return err
	}

	version, err := cmd.PersistentFlags().GetString(releaseVersionFlag)
	if err != nil {
		return err
	}

	return r.Delete(cmd.Context(), packageID, tag, version)
}
