package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"

	"github.com/spf13/cobra"

	"go.maczukin.dev/tools/greleaser"
)

var (
	rootCmd = &cobra.Command{
		Use:     "greleaser",
		Short:   "GReleaser is a simple tool for releasing projects through GitLab Release and Generic Package repository",
		Version: greleaser.AppVersion.SimpleLine(),
	}

	versionCmd = &cobra.Command{
		Use:   "version",
		Short: "Show extended version information",
		Run:   printVersion,
	}
)

func main() {
	createCmd, err := getCreateCmd()
	handleError(err)
	deleteCmd, err := getDeleteCmd()
	handleError(err)

	rootCmd.Context()
	rootCmd.AddCommand(createCmd)
	rootCmd.AddCommand(deleteCmd)
	rootCmd.AddCommand(versionCmd)

	ctx, cancelFn := signal.NotifyContext(context.Background(), os.Interrupt)
	defer cancelFn()

	err = rootCmd.ExecuteContext(ctx)
	handleError(err)
}

func handleError(err error) {
	if err == nil {
		return
	}

	_, _ = fmt.Fprintln(os.Stderr, err)
	os.Exit(1)
}

func printVersion(_ *cobra.Command, _ []string) {
	greleaser.AppVersion.PrintExtended()
}
