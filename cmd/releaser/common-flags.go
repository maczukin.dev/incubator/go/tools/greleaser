package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

const (
	gitlabURLFlag  = "gitlab-url"
	projectIDFlag  = "project-id"
	projectURLFlag = "project-url"

	ciServerURLEnv  = "CI_SERVER_URL"
	ciProjectIDEnv  = "CI_PROJECT_ID"
	ciProjectURLEnv = "CI_PROJECT_URL"

	defaultGitLabURL = "https://gitlab.com"
)

func addCommonFlags(cmd *cobra.Command) error {
	projectID, err := getInt64Env(ciProjectIDEnv, -1)
	if err != nil {
		return err
	}

	cmd.PersistentFlags().String(gitlabURLFlag, getEnv(ciServerURLEnv, defaultGitLabURL), "URL of the GitLab instance")
	cmd.PersistentFlags().Int64(projectIDFlag, projectID, "ID of the project at GitLab instance")
	cmd.PersistentFlags().String(projectURLFlag, os.Getenv(ciProjectURLEnv), "GitLab URL of the project")

	return nil
}

func getEnv(env string, defaultVal string) string {
	v := os.Getenv(env)
	if v != "" {
		return v
	}

	return defaultVal
}

func getInt64Env(env string, defaultVal int64) (int64, error) {
	v := os.Getenv(env)
	if v == "" {
		return defaultVal, nil
	}

	val, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return -1, fmt.Errorf("parsing %s %q: %w", env, v, err)
	}

	return val, nil
}
