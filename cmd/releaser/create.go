package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"go.maczukin.dev/tools/greleaser"
	"go.maczukin.dev/tools/greleaser/gpg"
)

const (
	workdirFlag     = "workdir"
	packageNameFlag = "package-name"
	tagFlag         = "tag"
	versionFlag     = "version"

	gpgKeyFlag      = "gpg-key"
	gpgKeyFileFlag  = "gpg-key-file"
	gpgPassFlag     = "gpg-password"
	gpgPassFileFlag = "gpg-password-file"

	ciCommitTagEnv = "CI_COMMIT_TAG"

	gpgKeyEnv      = "GPG_KEY"
	gpgKeyFileEnv  = "GPG_KEY_FILE"
	gpgPassEnv     = "GPG_PASSWORD"
	gpgPassFileEnv = "GPG_PASSWORD_FILE"

	defaultWorkdir     = "builds"
	defaultPackageName = "package"
)

func getCreateCmd() (*cobra.Command, error) {
	cmd := &cobra.Command{
		Use:   "create",
		Short: "Create the release",
		RunE:  createRelease,
	}

	err := addCommonFlags(cmd)
	if err != nil {
		return nil, err
	}

	cmd.PersistentFlags().String(gpgKeyFlag, os.Getenv(gpgKeyEnv), "Armored GPG private key for release checksums file signing. If present, GReleaser will try to sign the file.")
	cmd.PersistentFlags().String(gpgKeyFileFlag, os.Getenv(gpgKeyFileEnv), fmt.Sprintf("Path to the file with the armored GPG private key. Use instead of %s if key is stored in a file", gpgKeyFlag))
	cmd.PersistentFlags().String(gpgPassFlag, os.Getenv(gpgPassEnv), "Password to decode the GPG key")
	cmd.PersistentFlags().String(gpgPassFileFlag, os.Getenv(gpgPassFileEnv), fmt.Sprintf("Path to the file with the password to the GPG key. Use instead of %s if password is stored in a file", gpgPassEnv))

	cmd.PersistentFlags().String(workdirFlag, defaultWorkdir, "Directory storing files that should be use to package the release")
	cmd.PersistentFlags().String(packageNameFlag, defaultPackageName, "Package name used when uploading to Generic Package registry")
	cmd.PersistentFlags().String(tagFlag, os.Getenv(ciCommitTagEnv), "Tag related to the release. When available, GReleaser will try to create a GitLab Release entry. If used, the tag must exists in the repository.")
	cmd.PersistentFlags().String(versionFlag, "", "Version of the release")

	return cmd, nil
}

func createRelease(cmd *cobra.Command, _ []string) error {
	r, err := getReleaser(cmd)
	if err != nil {
		return err
	}

	keyProvider, err := getKeyProvider(cmd)
	if err != nil {
		return err
	}

	workdir, err := cmd.PersistentFlags().GetString(workdirFlag)
	if err != nil {
		return err
	}

	packageName, err := cmd.PersistentFlags().GetString(packageNameFlag)
	if err != nil {
		return err
	}

	version, err := cmd.PersistentFlags().GetString(versionFlag)
	if err != nil {
		return err
	}

	tag, err := cmd.PersistentFlags().GetString(tagFlag)
	if err != nil {
		return err
	}

	return r.Create(cmd.Context(), keyProvider, workdir, packageName, version, tag)
}

func getReleaser(cmd *cobra.Command) (*greleaser.Release, error) {
	gitlabURL, err := cmd.PersistentFlags().GetString(gitlabURLFlag)
	if err != nil {
		return nil, err
	}

	projectID, err := cmd.PersistentFlags().GetInt64(projectIDFlag)
	if err != nil {
		return nil, err
	}

	projectURL, err := cmd.PersistentFlags().GetString(projectURLFlag)
	if err != nil {
		return nil, err
	}

	r := greleaser.New(gitlabURL, projectID, projectURL)

	return r, nil
}

func getKeyProvider(cmd *cobra.Command) (*gpg.DefaultKeyProvider, error) {
	gpgKey, err := cmd.PersistentFlags().GetString(gpgKeyFlag)
	if err != nil {
		return nil, err
	}

	gpgKeyFile, err := cmd.PersistentFlags().GetString(gpgKeyFileFlag)
	if err != nil {
		return nil, err
	}

	gpgPass, err := cmd.PersistentFlags().GetString(gpgPassFlag)
	if err != nil {
		return nil, err
	}

	gpgPassFile, err := cmd.PersistentFlags().GetString(gpgPassFileFlag)
	if err != nil {
		return nil, err
	}

	keyProvider := gpg.NewDefaultKeyProvider(gpgKey, gpgKeyFile, gpgPass, gpgPassFile)
	err = keyProvider.Load()
	if err != nil {
		return nil, err
	}

	return keyProvider, nil
}
