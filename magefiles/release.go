package main

import (
	"context"
	"fmt"
	"os"
	"strconv"

	"github.com/magefile/mage/sh"

	"go.maczukin.dev/tools/greleaser"
)

const (
	defaultWorkdir     = buildsDir
	defaultPackageName = "release-package"

	gpgKeyEnv          = "GPG_KEY"
	gpgPasswordFileEnv = "GPG_PASSWORD_FILE"

	packageNameEnv = "PACKAGE_NAME"

	ciServerURLEnv  = "CI_SERVER_URL"
	ciProjectURLEnv = "CI_PROJECT_URL"
	ciProjectIDEnv  = "CI_PROJECT_ID"

	defaultGitLabURL  = "https://gitlab.com"
	defaultProjectURL = "https://gitlab.com/maczukin.dev/incubator/go/tools/greleaser"
	defaultProjectID  = 42529366
)

// CreateRelease handles release creation, which includes generating and optionally
// signing checksums file, uploading release files as generic packages and optionally
// creating a GitLab Release entry
func CreateRelease(ctx context.Context) error {
	args := []string{
		"create",
		"--gpg-key-file", os.Getenv(gpgKeyEnv),
		"--gpg-password-file", os.Getenv(gpgPasswordFileEnv),
		"--workdir", defaultWorkdir,
		"--package-name", getPackageName(),
		"--version", getVersion(),
	}

	return sh.RunV(getGReleaserBin(), args...)
}

// DeleteRelease handles release deletion, which covers removing the uploaded generic package
func DeleteRelease(ctx context.Context) error {
	r, err := getReleaser()
	if err != nil {
		return err
	}

	packageID, err := getInt64Env(greleaser.ReleasePackageIDEnv, -1)
	if err != nil {
		return err
	}

	tag := getEnv(greleaser.ReleaseTagEnv, "")
	version := getEnv(greleaser.ReleaseVersionEnv, "")

	return r.Delete(ctx, packageID, tag, version)
}

func getPackageName() string {
	return getEnv(packageNameEnv, defaultPackageName)
}

func getEnv(env string, defaultVal string) string {
	v := os.Getenv(env)
	if v == "" {
		return defaultVal
	}

	return v
}

func getReleaser() (*greleaser.Release, error) {
	gitlabURL := getEnv(ciServerURLEnv, defaultGitLabURL)
	projectURL := getEnv(ciProjectURLEnv, defaultProjectURL)

	projectID, err := getInt64Env(ciProjectIDEnv, defaultProjectID)
	if err != nil {
		return nil, err
	}

	r := greleaser.New(gitlabURL, projectID, projectURL)

	return r, nil
}

func getInt64Env(env string, defaultVal int64) (int64, error) {
	v := os.Getenv(env)
	if v == "" {
		return defaultVal, nil
	}

	val, err := strconv.ParseInt(v, 10, 64)
	if err != nil {
		return -1, fmt.Errorf("parsing %s %q: %w", env, v, err)
	}

	return val, nil
}
