//go:build mage

package main

import (
	"fmt"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	"github.com/magefile/mage/sh"
)

const (
	binaryName = "greleaser"
	buildsDir  = "builds"
)

// Compile builds the binary
func Compile() error {
	env := map[string]string{
		"CGO_ENABLED": "0",
	}

	target := getGReleaserBin()

	args := []string{
		"build",
		"-o", target,
		"-ldflags", strings.Join(
			[]string{
				fmt.Sprintf("-X main.VERSION=%s", getVersion()),
				fmt.Sprintf("-X main.REVISION=%s", getRevision()),
				fmt.Sprintf("-X main.REF=%s", getReference()),
				fmt.Sprintf("-X main.BUILT=%s", time.Now().Format(time.RFC3339)),
				"-w",
			},
			" ",
		),
		"./cmd/releaser",
	}

	fmt.Printf("Compiling %s... ", target)

	err := sh.RunWithV(env, "go", args...)
	if err != nil {
		return fmt.Errorf("executing build command: %w", err)
	}

	fmt.Printf("DONE\n")

	return nil
}

func getGReleaserBin() string {
	target := filepath.Join(buildsDir, fmt.Sprintf("%s-%s-%s", binaryName, getGOOS(), getGOARCH()))
	if getGOOS() == "windows" {
		target += ".exe"
	}

	return target
}

func getGOOS() string {
	return getEnv("GOOS", runtime.GOOS)
}

func getGOARCH() string {
	return getEnv("GOARCH", runtime.GOARCH)
}
