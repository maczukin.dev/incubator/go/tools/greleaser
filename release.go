package greleaser

import (
	"context"
	"fmt"
	"os"

	"go.maczukin.dev/tools/greleaser/internal/gpg"
	"go.maczukin.dev/tools/greleaser/internal/strategy"
)

const (
	ReleaseEnvFile = "release.env"

	ReleasePackageIDEnv = "RELEASE_PACKAGE_ID"
	ReleaseTagEnv       = "RELEASE_TAG"
	ReleaseVersionEnv   = "RELEASE_VERSION"
)

type Release struct {
	gitlabBaseURL string
	projectID     int64
	projectURL    string
}

func New(gitlabBaseURL string, projectID int64, projectURL string) *Release {
	return &Release{
		gitlabBaseURL: gitlabBaseURL,
		projectID:     projectID,
		projectURL:    projectURL,
	}
}

// Create Will create the release. This includes generating checksums file, optionally signing that file
// (if GPG key is available) and uploading the files as a Generic Package to GitLab.
//
// keyProvider argument injects a gpg.KeyProvider interface. If not nil and if the provided key is
// not empty, checksums file will be signed.
//
// workdir argument points to the directory that contains files that should be packaged as the release.
//
// packageName argument defines the package name that will be used for the uploaded Generic Package.
//
// version argument defines the version used for the uploaded Generic Package and the GitLab Release entry.
//
// If tag argument is not empty, it will also create GitLab Release entry for the given tag (tag must
// exist in the repository).
//
// As a result, method creates a release.env file which stores information about the created release:
// - the Version value for the generated release (stored as RELEASE_VERSION),
// - the Package ID for the uploaded Generic Package (stored as RELEASE_PACKAGE_ID),
// - the tag used for the GitLab Release entry (stored as RELEASE_TAG).
//
// This file may be next stored as GitLab's dotenv report artifact and used for the environment stop
// job to delete the package and/or the release.
func (r *Release) Create(ctx context.Context, keyProvider gpg.KeyProvider, workdir string, packageName string, version string, tag string) error {
	mgr := r.newMgr()

	err := mgr.GenerateChecksumsFile(workdir)
	if err != nil {
		return err
	}

	err = mgr.SignChecksumsFile(keyProvider)
	if err != nil {
		return err
	}

	releaseEnvF, err := os.Create(ReleaseEnvFile)
	if err != nil {
		return fmt.Errorf("creating %q: %w", ReleaseEnvFile, err)
	}
	defer releaseEnvF.Close()

	_, err = fmt.Fprintf(releaseEnvF, "%s=%s\n", ReleaseVersionEnv, version)
	if err != nil {
		return fmt.Errorf("writing %s to %q: %w", ReleaseVersionEnv, ReleaseEnvFile, err)
	}

	packageID, err := mgr.UploadReleasePackageFiles(ctx, workdir, packageName, version)
	if err != nil {
		return err
	}

	fmt.Printf("Uploaded package with ID: %d\n", packageID)
	_, err = fmt.Fprintf(releaseEnvF, "%s=%d\n", ReleasePackageIDEnv, packageID)
	if err != nil {
		return fmt.Errorf("writing %s to %q: %w", ReleasePackageIDEnv, ReleaseEnvFile, err)
	}

	if tag != "" {
		err = mgr.CreateGitLabRelease(ctx, tag, r.projectURL, version)
		if err != nil {
			return err
		}

		_, err = fmt.Fprintf(releaseEnvF, "%s=%s\n", ReleaseTagEnv, tag)
		if err != nil {
			return fmt.Errorf("writing %s to %q: %w", ReleaseTagEnv, ReleaseEnvFile, err)
		}
	}

	_ = releaseEnvF.Close()

	return nil
}

func (r *Release) newMgr() *strategy.Manager {
	return strategy.NewManager(r.gitlabBaseURL, r.projectID)
}

func (r *Release) Delete(ctx context.Context, packageID int64, tag string, version string) error {
	mgr := r.newMgr()
	err := mgr.DeleteReleasePackage(ctx, packageID, version)
	if err != nil {
		return err
	}

	if tag != "" {
		err = mgr.DeleteGitLabRelease(ctx, tag)
		if err != nil {
			return err
		}
	}

	return nil
}
