package gpg

import (
	"fmt"
	"os"
)

type DefaultKeyProvider struct {
	key      string
	keyFile  string
	pass     string
	passFile string
}

func NewDefaultKeyProvider(key string, keyFile string, pass string, passFile string) *DefaultKeyProvider {
	return &DefaultKeyProvider{
		key:      key,
		keyFile:  keyFile,
		pass:     pass,
		passFile: passFile,
	}
}

func (p *DefaultKeyProvider) Load() error {
	err := p.loadFiles()
	if err != nil {
		return fmt.Errorf("loading GPG key: %w", err)
	}

	return nil
}

func (p *DefaultKeyProvider) loadFiles() error {
	if p.keyFile != "" {
		data, err := os.ReadFile(p.keyFile)
		if err != nil {
			return fmt.Errorf("reading key file %q: %w", p.keyFile, err)
		}

		p.key = string(data)
	}

	if p.passFile != "" {
		data, err := os.ReadFile(p.passFile)
		if err != nil {
			return fmt.Errorf("reading password file %q: %w", p.passFile, err)
		}

		p.key = string(data)
	}

	return nil
}

func (p *DefaultKeyProvider) Key() string {
	return p.key
}

func (p *DefaultKeyProvider) Password() string {
	return p.pass
}
