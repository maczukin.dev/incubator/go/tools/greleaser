package gpg

import (
	"fmt"
	"os"
)

type EnvKeyProvider struct {
	keyEnv      string
	keyFileEnv  string
	passEnv     string
	passFileEnv string

	key  string
	pass string
}

func NewEnvKeyProvider(keyEnv string, keyFileEnv string, passEnv string, passFileEnv string) *EnvKeyProvider {
	return &EnvKeyProvider{
		keyEnv:      keyEnv,
		keyFileEnv:  keyFileEnv,
		passEnv:     passEnv,
		passFileEnv: passFileEnv,
	}
}

func (p *EnvKeyProvider) ParseEnvs() error {
	err := p.parseKey()
	if err != nil {
		return fmt.Errorf("parsing GPG key environment variables: %w", err)
	}

	return p.parsePass()
}

func (p *EnvKeyProvider) parseKey() error {
	key := os.Getenv(p.keyEnv)
	if key == "" {
		keyFile := os.Getenv(p.keyFileEnv)
		if keyFile == "" {
			return nil
		}

		keyBytes, err := os.ReadFile(keyFile)
		if err != nil {
			return fmt.Errorf("reading GPG key file %q: %w", keyFile, err)
		}

		key = string(keyBytes)
	}

	p.key = key

	return nil
}

func (p *EnvKeyProvider) parsePass() error {
	pass := os.Getenv(p.passEnv)
	if pass == "" {
		passFile := os.Getenv(p.passFileEnv)
		if passFile != "" {
			passBytes, err := os.ReadFile(passFile)
			if err != nil {
				return fmt.Errorf("reading GPG password file %q: %w", passFile, err)
			}

			pass = string(passBytes)
		}
	}

	p.pass = pass

	return nil
}

func (p *EnvKeyProvider) Key() string {
	return p.key
}

func (p *EnvKeyProvider) Password() string {
	return p.pass
}
