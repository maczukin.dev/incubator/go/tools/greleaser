package greleaser

import (
	"fmt"
	"runtime"
)

var (
	VERSION  string = "latest"
	REVISION string = "HEAD"
	REF      string = "-"
	BUILT    string = "now"

	AppVersion = new(appVersion)
)

type appVersion struct{}

func (a *appVersion) SimpleLine() string {
	return fmt.Sprintf("%s (%s)", VERSION, REVISION)
}

func (a *appVersion) PrintExtended() {
	fmt.Printf("Version:   %s\n", VERSION)
	fmt.Printf("Revision:  %s\n", REVISION)
	fmt.Printf("Reference: %s\n", REF)
	fmt.Printf("Built:     %s\n", BUILT)
	fmt.Printf("GoVersion: %s\n", runtime.Version())
	fmt.Printf("OS:        %s\n", runtime.GOOS)
	fmt.Printf("Arch:      %s\n", runtime.GOARCH)
}
