package packages

import (
	"context"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"go.maczukin.dev/tools/greleaser/internal/gitlab"
)

type Uploader struct {
	gitlab *gitlab.Client

	projectID   int64
	packageName string
	version     string

	workdir string
}

type UploadResult struct {
	PackageID int64
	Assets    []UploadResultAsset
}

type UploadResultAsset struct {
	Name   string
	Path   string
	FileID int64
}

func NewUploader(gitlab *gitlab.Client, projectID int64, packageName string, version string, workdir string) *Uploader {
	return &Uploader{
		gitlab:      gitlab,
		projectID:   projectID,
		packageName: packageName,
		version:     version,
		workdir:     workdir,
	}
}

func (u *Uploader) Upload(ctx context.Context) (UploadResult, error) {
	var result UploadResult

	fmt.Printf("Uploading files for package %q in version %s:\n", u.packageName, u.version)
	err := filepath.Walk(u.workdir, u.walkDirFn(ctx, &result))
	if err != nil {
		return result, fmt.Errorf("scaning directory %q: %w", u.workdir, err)
	}

	return result, nil
}

func (u *Uploader) walkDirFn(ctx context.Context, result *UploadResult) filepath.WalkFunc {
	return func(p string, info fs.FileInfo, _ error) error {
		if info.IsDir() {
			return nil
		}

		fileName := strings.ReplaceAll(u.trimWorkdirPrefix(p), string(filepath.Separator), "_")
		fmt.Printf("- %s as %s... ", p, fileName)

		f, err := os.Open(p)
		if err != nil {
			return fmt.Errorf("opening file: %w", err)
		}

		defer f.Close()

		r, err := u.gitlab.PackageFileUpload(ctx, u.projectID, u.packageName, u.version, fileName, f)
		if err != nil {
			return fmt.Errorf("uploading file: %w", err)
		}

		_ = f.Close()

		result.PackageID = r.PackageID
		result.Assets = append(result.Assets, UploadResultAsset{
			Name:   info.Name(),
			Path:   u.trimWorkdirPrefix(p),
			FileID: r.ID,
		})

		fmt.Printf("DONE\n")

		return nil
	}
}

func (u *Uploader) trimWorkdirPrefix(path string) string {
	return strings.TrimPrefix(path, fmt.Sprintf("%s%c", u.workdir, filepath.Separator))
}
