package packages

import (
	"context"
	"fmt"

	"go.maczukin.dev/tools/greleaser/internal/gitlab"
)

type Deleter struct {
	gitlab *gitlab.Client

	projectID int64
	packageID int64
	version   string
}

func NewDeleter(gitlab *gitlab.Client, projectID int64, packageID int64, version string) *Deleter {
	return &Deleter{
		gitlab:    gitlab,
		projectID: projectID,
		packageID: packageID,
		version:   version,
	}
}

func (d *Deleter) Delete(ctx context.Context) error {
	fmt.Printf("Deleting Release for %s... ", d.version)

	err := d.gitlab.PackageDelete(ctx, d.projectID, d.packageID)
	if err != nil {
		return fmt.Errorf("deleting package: %w", err)
	}

	fmt.Printf("DONE\n")

	return nil
}
