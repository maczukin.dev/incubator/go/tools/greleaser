package gpg

import (
	"bytes"
	"fmt"
	"os"
	"strings"

	"golang.org/x/crypto/openpgp"
)

type KeyProvider interface {
	Key() string
	Password() string
}

type Signer struct {
	entity *openpgp.Entity
}

func NewSigner(keyProvider KeyProvider) (*Signer, error) {
	entities, err := openpgp.ReadArmoredKeyRing(bytes.NewBufferString(keyProvider.Key()))
	if err != nil {
		return nil, fmt.Errorf("parsing armored keyring: %w", err)
	}

	if len(entities) < 1 {
		return nil, fmt.Errorf("no keys found in the keyring")
	}

	entity := entities[0]
	if entity.PrivateKey == nil {
		return nil, fmt.Errorf("missing private key in the keyring")
	}

	err = entity.PrivateKey.Decrypt([]byte(strings.TrimSpace(keyProvider.Password())))
	if err != nil {
		return nil, fmt.Errorf("decrypting private key: %w", err)
	}

	signer := &Signer{
		entity: entity,
	}

	return signer, nil
}

func (s *Signer) Sign(sourcePath string) error {
	fmt.Printf("Signing %q... ", sourcePath)

	targetPath := fmt.Sprintf("%s.asc", sourcePath)

	sourceFile, err := os.Open(sourcePath)
	if err != nil {
		return fmt.Errorf("opening file for signing %q: %w", sourcePath, err)
	}
	defer sourceFile.Close()

	targetFile, err := os.Create(targetPath)
	if err != nil {
		return fmt.Errorf("creating signature file %q: %w", targetPath, err)
	}
	defer targetFile.Close()

	err = openpgp.ArmoredDetachSign(targetFile, s.entity, sourceFile, nil)
	if err != nil {
		return fmt.Errorf("preparing detached GPG signature: %w", err)
	}

	_ = targetFile.Close()
	_ = sourceFile.Close()

	fmt.Printf("DONE\n")

	return nil
}
