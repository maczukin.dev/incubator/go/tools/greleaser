package release

import (
	"context"
	"fmt"

	"go.maczukin.dev/tools/greleaser/internal/gitlab"
	"go.maczukin.dev/tools/greleaser/internal/packages"
)

type Creator struct {
	gitlab *gitlab.Client

	projectID  int64
	projectURL string
	version    string
	tag        string
}

func NewCreator(gitlab *gitlab.Client, projectID int64, projectURL string, version string, tag string) *Creator {
	return &Creator{
		gitlab:     gitlab,
		projectID:  projectID,
		projectURL: projectURL,
		version:    version,
		tag:        tag,
	}
}

func (c *Creator) Create(ctx context.Context, assets []packages.UploadResultAsset) error {
	fmt.Printf("Creating GitLab Release for %s\n", c.version)
	fmt.Printf("Tag: %s\n", c.tag)

	glRelease := gitlab.GLRelease{
		Name:    c.version,
		TagName: c.tag,
		Assets: gitlab.GLReleaseAsset{
			Links: make([]gitlab.GLReleaseAssetLink, 0),
		},
	}

	for _, asset := range assets {
		glRelease.Assets.Links = append(glRelease.Assets.Links, gitlab.GLReleaseAssetLink{
			Name:     asset.Name,
			URL:      fmt.Sprintf("%s/-/package_files/%d/download", c.projectURL, asset.FileID),
			Filepath: fmt.Sprintf("/%s", asset.Path),
		})
	}

	err := c.gitlab.GitLabReleaseCreate(ctx, c.projectID, glRelease)
	if err != nil {
		return fmt.Errorf("creating GitLab Release: %w", err)
	}

	return nil
}
