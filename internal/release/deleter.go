package release

import (
	"context"
	"fmt"

	"go.maczukin.dev/tools/greleaser/internal/gitlab"
)

type Deleter struct {
	gitlab *gitlab.Client

	projectID int64
	tag       string
}

func NewDeleter(gitlab *gitlab.Client, projectID int64, tag string) *Deleter {
	return &Deleter{
		gitlab:    gitlab,
		projectID: projectID,
		tag:       tag,
	}
}

func (d *Deleter) Delete(ctx context.Context) error {
	fmt.Printf("Deleting GitLab Release for %s\n", d.tag)

	err := d.gitlab.GitLabReleaseDelete(ctx, d.projectID, d.tag)
	if err != nil {
		return fmt.Errorf("deleting GitLab Release: %w", err)
	}

	return nil
}
