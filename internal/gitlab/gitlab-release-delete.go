package gitlab

import (
	"context"
	"fmt"
	"net/http"
)

func (c *Client) GitLabReleaseDelete(ctx context.Context, projectID int64, tag string) error {
	u := fmt.Sprintf("%s/projects/%d/releases/%s", c.apiV4URL, projectID, tag)

	req, err := http.NewRequestWithContext(ctx, http.MethodDelete, u, nil)
	if err != nil {
		return fmt.Errorf("creting HTTP request: %w", err)
	}

	resp, err := c.do(req, http.StatusOK)
	if err != nil {
		return fmt.Errorf("executing HTTP request: %w", err)
	}

	c.flushBody(resp)

	return nil
}
