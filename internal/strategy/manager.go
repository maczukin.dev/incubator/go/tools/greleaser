package strategy

import (
	"context"
	"fmt"

	"go.maczukin.dev/tools/greleaser/internal/checksums"
	"go.maczukin.dev/tools/greleaser/internal/gitlab"
	"go.maczukin.dev/tools/greleaser/internal/gpg"
	"go.maczukin.dev/tools/greleaser/internal/packages"
	"go.maczukin.dev/tools/greleaser/internal/release"
)

const (
	checksumsFileName = "release.sha256"
)

type Manager struct {
	projectID int64

	gitlab *gitlab.Client

	checksumsFilePath string
	uploadResult      packages.UploadResult
}

func NewManager(gitlabBaseURL string, projectID int64) *Manager {
	return &Manager{
		projectID: projectID,
		gitlab:    gitlab.NewClient(gitlabBaseURL),
	}
}

func (m *Manager) GenerateChecksumsFile(workdir string) error {
	cg := checksums.NewGenerator(checksumsFileName, workdir)

	filePath, err := cg.Generate()
	if err != nil {
		return fmt.Errorf("generating checksums file: %w", err)
	}

	m.checksumsFilePath = filePath

	return nil
}

func (m *Manager) SignChecksumsFile(keyProvider gpg.KeyProvider) error {
	if keyProvider == nil || keyProvider.Key() == "" {
		return nil
	}

	s, err := gpg.NewSigner(keyProvider)
	if err != nil {
		return fmt.Errorf("creating GPG signer: %w", err)
	}

	err = s.Sign(m.checksumsFilePath)
	if err != nil {
		return fmt.Errorf("signing checksums file: %w", err)
	}

	return nil
}

func (m *Manager) UploadReleasePackageFiles(ctx context.Context, workdir string, packageName string, version string) (int64, error) {
	u := packages.NewUploader(m.gitlab, m.projectID, packageName, version, workdir)
	result, err := u.Upload(ctx)
	if err != nil {
		return -1, fmt.Errorf("uploading release package files: %w", err)
	}

	m.uploadResult = result

	return result.PackageID, nil
}

func (m *Manager) DeleteReleasePackage(ctx context.Context, packageID int64, version string) error {
	d := packages.NewDeleter(m.gitlab, m.projectID, packageID, version)
	err := d.Delete(ctx)
	if err != nil {
		return fmt.Errorf("deleting release package files: %w", err)
	}

	return nil
}

func (m *Manager) CreateGitLabRelease(ctx context.Context, tag string, projectURL string, version string) error {
	c := release.NewCreator(m.gitlab, m.projectID, projectURL, version, tag)
	err := c.Create(ctx, m.uploadResult.Assets)
	if err != nil {
		return fmt.Errorf("creating GitLab Release entry: %w", err)
	}

	return nil
}

func (m *Manager) DeleteGitLabRelease(ctx context.Context, tag string) error {
	d := release.NewDeleter(m.gitlab, m.projectID, tag)
	err := d.Delete(ctx)
	if err != nil {
		return fmt.Errorf("deleting GitLab Release entry: %w", err)
	}

	return nil
}
