package checksums

import (
	"crypto/sha256"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"sort"
	"strings"
)

type Generator struct {
	fileName string
	filePath string

	workdir string

	scannedFileNames []string
	checksums        map[string]string
}

func NewGenerator(fileName string, workdir string) *Generator {
	return &Generator{
		fileName:         fileName,
		filePath:         filepath.Join(workdir, fileName),
		workdir:          workdir,
		scannedFileNames: make([]string, 0),
		checksums:        make(map[string]string, 0),
	}
}

func (cg *Generator) Generate() (string, error) {
	fmt.Printf("Generating checksums file %q:\n", cg.filePath)

	err := filepath.Walk(cg.workdir, cg.dirWalkFn)
	if err != nil {
		return cg.filePath, fmt.Errorf("scaning directory %q: %w", cg.workdir, err)
	}

	sort.Strings(cg.scannedFileNames)

	return cg.filePath, cg.saveFile()
}

func (cg *Generator) dirWalkFn(p string, info fs.FileInfo, _ error) error {
	if info.IsDir() {
		return nil
	}

	if strings.Contains(info.Name(), cg.fileName) {
		return nil
	}

	fmt.Printf("- %s... ", p)

	f, err := os.Open(p)
	if err != nil {
		return fmt.Errorf("reading file: %w", err)
	}
	defer f.Close()

	hash := sha256.New()
	_, err = io.Copy(hash, f)
	if err != nil {
		return fmt.Errorf("calculating checksum: %w", err)
	}

	_ = f.Close()

	checksum := fmt.Sprintf("%x", hash.Sum(nil))

	fmt.Printf("sha256:%s\n", checksum)

	cg.checksums[cg.trimWorkdirPrefix(p)] = checksum
	cg.scannedFileNames = append(cg.scannedFileNames, cg.trimWorkdirPrefix(p))

	return nil
}

func (cg *Generator) trimWorkdirPrefix(path string) string {
	return strings.TrimPrefix(path, fmt.Sprintf("%s%c", cg.workdir, filepath.Separator))
}

func (cg *Generator) saveFile() error {
	f, err := os.Create(cg.filePath)
	if err != nil {
		return fmt.Errorf("creating checksums file %q: %w", cg.filePath, err)
	}
	defer f.Close()

	for _, file := range cg.scannedFileNames {
		_, ok := cg.checksums[file]
		if !ok {
			continue
		}

		_, err = fmt.Fprintf(f, "%s\t%s\n", cg.checksums[file], file)
		if err != nil {
			return fmt.Errorf("writing to checksums file %q: %w", cg.filePath, err)
		}
	}

	_ = f.Close()

	return nil
}
